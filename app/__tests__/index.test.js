import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'

 
// InputPhone.test.tsx
import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { InputPhone }  from '../components/InputPhone'

describe('InputPhone component', () => {
  it('renders without crashing', async () => {
    render(<InputPhone prefixCountryCallback={() => {}} />);
    // You might want to add more specific assertions based on your component's structure
    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });

  it('handles user input and selection', async () => {
    render(<InputPhone prefixCountryCallback={() => {}} />);
    await waitFor(() => screen.getByText('An error has occurred')); // Assuming an error state is displayed on error

    // Mock a successful API response
    jest.spyOn(global, 'fetch').mockResolvedValue({
      json: jest.fn().mockResolvedValue({ /* Mock your API response data here */ }),
    } as any);

    // Simulate user typing
    const inputElement = screen.getByRole('textbox');
    fireEvent.change(inputElement, { target: { value: 'Some text' } });

    // Simulate arrow down key press
    fireEvent.keyDown(inputElement, { key: 'ArrowDown' });

    // Assert that the selected country has changed
    expect(screen.getByText('Selected Country')).toBeInTheDocument();
  });

  // Add more test cases as needed
});
