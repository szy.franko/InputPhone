// InputPhone.test.tsx
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // For extended DOM matchers
import { act } from 'react-dom/test-utils'; // For async testing
import InputPhone from './InputPhone';

// Mock the useQuery hook
jest.mock('react-query');

describe('InputPhone', () => {
  test('renders component', () => {
    render(<InputPhone prefixCountryCallback={() => {}} />);
    expect(screen.getByTestId('input-phone')).toBeInTheDocument();
  });

  test('handles user input', async () => {
    const prefixCountryCallback = jest.fn();
    render(<InputPhone prefixCountryCallback={prefixCountryCallback} />);

    // Simulate user input
    const inputElement = screen.getByLabelText('Your Label'); // Adjust the label text
    await act(async () => {
      fireEvent.change(inputElement, { target: { value: 'New Value' } });
    });

    // Add assertions based on your component's behavior
    expect(prefixCountryCallback).toHaveBeenCalledWith(/* expected arguments */);
  });

  // Add more tests based on your component's functionality
});
    