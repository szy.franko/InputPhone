import React, { FC, useState, useEffect, useRef, KeyboardEvent, WheelEvent } from 'react';
import { useQuery, UseQueryResult } from 'react-query';
import './InputPhone.css';
import { PhoneData } from '../interfaces/';

interface Params {
  header?: string;
  subheader?: string;
  prefixCountryCallback: (data: PhoneData) => void;
}

interface ResponseData {
  [key: string]: Country;
}

interface Country {
  name: string;
  region: string;
  timezones: object;
  iso: object;
  phone: string[];
  emoji: string;
  image: string;
}

const LOADING_MESSAGE = 'Loading...';
const ERROR_MESSAGE = 'An error has occurred';

const InputPhone: FC<Params> = ({ header, subheader, prefixCountryCallback }: Params) => {
  const [prefix, setPrefix] = useState<string>('');
  const [selectedCountry, setSelectedCountry] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const select = useRef<HTMLUListElement>(null);

  let resp: UseQueryResult<ResponseData, object> = useQuery('repoData', () =>
    fetch(`http://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/countries/prefix/${prefix || 'null'}`).then(
      (res) => res.json()
    )
  );

  useEffect(() => {
    if (!resp.data || Object.keys(resp.data).length === 0) return;

    const firstKey = Object.keys(resp.data)[0];
    if (Object.keys(resp.data).length === 1 && phone.match(/[0-9]{3}-[0-9]{3}-[0-9]{3}/)) {
      prefixCountryCallback({
        prefix: resp.data[firstKey].phone[0],
        phone,
      });
    }
  }, [resp.data, phone]);

  useEffect(() => {
    resp.refetch();
  }, [prefix, selectedCountry]);

  if (resp.isLoading) return <div>{LOADING_MESSAGE}</div>;

  if (resp.error) return <div>{ERROR_MESSAGE}</div>;

  const increaseCountryIndex = () => {
    if (!resp.data) return;
    const countries = Object.keys(resp.data);
    const index = countries.indexOf(selectedCountry);
    if (index === countries.length - 1) {
      setSelectedCountry(Object.keys(resp.data)[0])
    } else {
      setSelectedCountry(countries[index + 1]);
    }
  };

  const decreaseCountryIndex = () => {
    if (!resp.data) return;
    const countries = Object.keys(resp.data);
    const index = countries.indexOf(selectedCountry);
    if (index === 0) {
      setSelectedCountry(Object.keys(resp.data).at(-1) as string);
    } else {
      setSelectedCountry(countries[index - 1]);
    }
  };

  const changePrefix = async ($event: KeyboardEvent<HTMLElement>) => {
    if (!resp.data) return;
    const { key } = $event;
    
    switch (key) {
      case 'Backspace':
      case 'Escape':
        await setPrefix('');
        break;
  
      case 'ArrowDown':
        increaseCountryIndex();
        break;
  
      case 'ArrowUp':
        decreaseCountryIndex();
        break;
  
      case 'Enter':
        const countries = Object.keys(resp.data);
        const index = countries.indexOf(selectedCountry);
        setPrefix(resp.data[countries[index]].name);
        setSelectedCountry(countries[index]);
        break;
  
      default:
        if (key.match(/^[A-Za-z]$/)) {
          await setPrefix(prefix + key);
        }
        break;
    }
  };

  const focusSelect = () => {
    if (select && select.current) {
      select.current.focus();
    }
  };

  const blurSelect = () => {
    if (select && select.current) {
      select.current.blur();
    }
  };

  const handleInput = ($event: KeyboardEvent<HTMLInputElement>) => {
    const { currentTarget, key } = $event;

    $event.preventDefault();
    if (key.match(/^[A-Za-z]$/)) {
      $event.preventDefault();
      changePrefix($event);
      return;
    }

    if (key === 'Backspace') {
      currentTarget.value = currentTarget.value.slice(0, -1);
      return;
    }

    if (key.match(/[A-Za-z]{2}/)) {
      return;
    }

    if (
      currentTarget.value.match(/^[0-9]{3}-[0-9]{3}-[0-9]{2}$/) &&
      key.match(/^[0-9]$/)
    ) {
      setPhone(currentTarget.value + key);
    }

    if (
      currentTarget.value.match(/^[0-9]{3}-[0-9]{3}-[0-9]{3}$/) &&
      key.match(/^[0-9]$/)
    ) {
      $event.preventDefault();
      return;
    }

    if (currentTarget.value.match(/[0-9]{3}$/)) {
      if (key.match(/[0-9]/)) {
        currentTarget.value = (currentTarget.value + '-' + key);
        return;
      }
    }

    if (currentTarget.value.match(/[!@#$%^&*()]$/)) {
      currentTarget.value = currentTarget.value.slice(0, -1);
      return;
    }

    currentTarget.value += key;
  };

  const handleClickCountry = (el: string) => {
    if (!resp.data) return;
    if (Object.keys(resp.data).length === 1) {
      setPrefix(prefix[0]);
    } else {
      setPrefix(resp.data[el].name);
      setSelectedCountry(el);
    }
  };

  const handleScrollCountry = ($event: WheelEvent<HTMLUListElement>) => {
    if ($event.deltaY > 0) {
      increaseCountryIndex();
    } else if ($event.deltaY < 0) {
      decreaseCountryIndex();
    }
  };

  return (
    <div className="input-phone dialog">
      {phone}
      <p className="header">{header}</p>
      <label className="label">
        {subheader}
        <div className="input-wrapper">
          <ul
            id="select"
            className="select"
            tabIndex={0}
            ref={select}
            onMouseOver={focusSelect}
            onMouseLeave={blurSelect}
            onWheel={($event) => {
              handleScrollCountry($event);
            }}
            onKeyDown={($event) => {
              changePrefix($event);
            }}
          >
            {resp.data && Object.keys(resp.data).length === 0 && (
              <div className="ask">{'Input your country name:' + prefix}</div>
            )}
            {resp.data && Object.keys(resp.data).length===0 && prefix.length>0 && 
              <p style={{fontSize: '10px', textAlign: 'center'}}>Country withe a given <br /> prefix does not exist</p>}
            {resp.data && Object.keys(resp.data).map((el, index) => {
              return (
                <li
                  key={index}
                  onClick={() => {
                    handleClickCountry(el);
                  }}
                  onMouseEnter={() => {
                    setSelectedCountry(el);
                  }}
                  onMouseLeave={() => {
                    setSelectedCountry('');
                  }}
                  className={`${selectedCountry === el ? 'selected' : ''} country`}
                >
                  {resp.data && <img height={20} width={"auto"} src={resp.data[el].image} />}
                  {resp.data && resp.data[el].name} {resp.data && resp.data[el].phone[0]}
                </li>
              );
            })}
          </ul>
          <input
            onKeyDown={($event) => {
              handleInput($event);
            }}
            type="tel"
            pattern="([0-9]{3}-){2}[0-9]"
            className="input"
          />
        </div>
      </label>
    </div>
  );
};

export default InputPhone;
