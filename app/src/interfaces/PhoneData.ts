export interface PhoneData {
    prefix: string,
    phone: string
  };