import './App.css';
import { CSSProperties } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';

import { PhoneData } from './interfaces/';

import InputPhone from './components/InputPhone';




const queryClient: QueryClient = new QueryClient();

const appCss: CSSProperties = {
  height: '300px',
};

function App() {
  const prefixCountryCallback = (data:PhoneData) =>{
    alert(data.prefix+' '+data.phone);
  }
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App" style={appCss}>
        <InputPhone 
          header="Change phone number"
          subheader="Provide new phone number"
          prefixCountryCallback={prefixCountryCallback}
          />
      </div>
    </QueryClientProvider>
  );
}

export default App;
