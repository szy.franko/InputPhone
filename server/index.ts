import express, { Express, Request, Response } from 'express';
const cors = require('cors')
const fs = require('fs')
const app = express()
const port = 8080
const { exec } = require('child_process');

interface Country {
  [countryShort:string]: {
    name: string,
    region: string,
    timezones: object,
    iso: object,
    phone: string[],
    emoji: string,
    image: string
    }
};

const countries = JSON.parse(fs.readFileSync(__dirname+'/data/countries.json').toString());
const countriesWiki = JSON.parse(fs.readFileSync(__dirname+'/data/countries.json').toString());
app.use(cors());

app.get('/', (req:Request, res:Response) => {
  res.send('Hello World!')
})

app.get('/countries',(req:Request,res:Response)=>{
    res.json(countries);
})

app.get('/countries/prefix/:prefix',(req:Request,res: Response)=>{
  console.log(req.params)
  if(req.params.prefix==='null') {
    res.json({});
    return;
  };
  
  let filteredCountries = Object.keys(countries).filter(el=>
    countries[el].name.toLowerCase().startsWith(req.params.prefix.toLowerCase())
  );
  
  res.json(
    filteredCountries.reduce((obj, key) => {
      return Object.assign(obj, {
        [key]: countries[key]
      });
  }, {})  
  )
})

app.get('/download',()=>{
  for(let country in countries) {
    console.log(countries[country].image )
    countriesWiki[country].image =  `https://flagcdn.com/${country.toLowerCase()}.svg`;
    
  }


  const countriesWikiJSON = JSON.stringify(countriesWiki, null, 2); // Drugi argument (null) oznacza brak transformacji, a trzeci (2) to liczba spacji użytych do wcięć



fs.writeFile('./data/countriesWiki.json', countriesWikiJSON, 'utf8', (err:any) => {
  if (err) {
    console.error('Błąd podczas zapisywania pliku:', err);
    return;
  }
  console.log('Plik został zapisany.');
});
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})